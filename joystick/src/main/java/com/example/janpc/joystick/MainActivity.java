package com.example.janpc.joystick;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jmedeisis.bugstick.Joystick;
import com.jmedeisis.bugstick.JoystickListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;


public class MainActivity extends Activity {

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
                header.setText("CONNECTED");
                currentState = STATE.CONNECTED;
                bluetoothConnected();
                Log.d("bluetooth: ", "device connected"); //Device is now connected
            }
            else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
                isBtConnected = false;
                if (bluetoothAdapter.isEnabled()) {
                    header.setText("SELECT A DEVICE");
                    currentState = STATE.BLUETOOTH_ON;
                    selectDevice();
                } else {
                    header.setText("BLUETOOTH");
                    bluetoothOff();
                    currentState = STATE.BLUETOOTH_OFF;
                }

                Log.d("bluetooth: ", "device disconnected"); //Device has disconnected
            }

            final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                    BluetoothAdapter.ERROR);

            switch (state) {
                case BluetoothAdapter.STATE_OFF:
                    header.setText("BLUETOOTH");
                    bluetoothOff();
                    currentState = STATE.BLUETOOTH_OFF;
                    Log.d("bluetooth: ", "bluetooth off");
                    break;
                case BluetoothAdapter.STATE_TURNING_OFF:
                    Log.d("bluetooth: ", "bluetooth turning off");
                    break;
                case BluetoothAdapter.STATE_ON:
                    header.setText("SELECT A DEVICE");
                    selectDevice();
                    currentState = STATE.BLUETOOTH_ON;
                    Log.d("bluetooth: ", "bluetooth on");
                    break;
                case BluetoothAdapter.STATE_TURNING_ON:
                    Log.d("bluetooth: ", "bluetooth turning on");
                    break;
            }

        }
    };

    ImageButton lock;
    ImageButton swap;
    ImageButton force;

    boolean locked = true;
    boolean swaped = false;
    boolean forced = true;

    public TextView header;
    ListView console;

    Joystick left;
    Joystick right;

    int offsetL=0;
    int offsetR=0;
    int angleL=0;
    int angleR=0;

    //bluetooth
    public BluetoothAdapter bluetoothAdapter;
    private Set<BluetoothDevice> pairedDevices;
    ArrayList<String> listItems=new ArrayList<String>();

    public BluetoothSocket bluetoothSocket = null;
    public boolean isBtConnected = false;
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    String deviceAddress;
    private ProgressDialog progress;

    public enum STATE {BLUETOOTH_OFF, BLUETOOTH_ON, CONNECTED}
    public static STATE currentState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lock = (ImageButton) findViewById(R.id.lock);
        lock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!locked) {
                    if(!swaped){
                        left.setMotionConstraint(Joystick.MotionConstraint.VERTICAL);
                        right.setMotionConstraint(Joystick.MotionConstraint.HORIZONTAL);
                    }else {
                        left.setMotionConstraint(Joystick.MotionConstraint.HORIZONTAL);
                        right.setMotionConstraint(Joystick.MotionConstraint.VERTICAL);
                    }

                    lock.setImageResource(R.drawable.lockedxxhdpi);

                } else {
                    left.setMotionConstraint(Joystick.MotionConstraint.NONE);
                    right.setMotionConstraint(Joystick.MotionConstraint.NONE);
                    lock.setImageResource(R.drawable.unlockedxxhdpi);
                }
                locked = !locked;
            }
        });


        swap = (ImageButton) findViewById(R.id.swap);
        swap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!swaped) {
                    swap.setImageResource(R.drawable.switch_offxxhdpi);
                } else {
                    swap.setImageResource(R.drawable.switch_onxxhdpi);
                }
                swaped = !swaped;
                lock.callOnClick();
                lock.callOnClick();
            }
        });

        force = (ImageButton) findViewById(R.id.force);
        force.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (forced) {
                    force.setImageResource(R.drawable.no_forcexxhdpi);
                    left.setStartOnFirstTouch(false);
                    right.setStartOnFirstTouch(false);
                } else {
                    force.setImageResource(R.drawable.forcexxhdpi);
                    left.setStartOnFirstTouch(true);
                    right.setStartOnFirstTouch(true);
                }
                forced = !forced;
            }
        });


        left = (Joystick) findViewById(R.id.joystick_left);
        left.setMotionConstraint(Joystick.MotionConstraint.VERTICAL);
        left.setJoystickListener(new JoystickListener() {
            @Override
            public void onDown() {
            }

            @Override
            public void onDrag(float degrees, float offset) {
                offsetL = (int) (63 * offset);
                angleL = (int) degrees;

                if (currentState.equals(STATE.CONNECTED)) {
                    updateJoystickData();
                }

                try {
                    if (currentState.equals(STATE.CONNECTED)) {
                        char posiljka;
                        if (swaped) {  //zavijanje, dodamo 128
                            posiljka = (char) (offsetL * Math.cos(Math.toRadians(angleL))); //value from -63 to 63
                            posiljka += 128+63;
                            bluetoothSocket.getOutputStream().write(posiljka);
                        } else {    //hitrost
                            posiljka = (char) (offsetL * Math.sin(Math.toRadians(angleL)));
                            posiljka += 63;
                            bluetoothSocket.getOutputStream().write(posiljka);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onUp() {
                offsetL = 0;
                angleL = 0;

                if (currentState.equals(STATE.CONNECTED)) {
                    updateJoystickData();
                }

                try {
                    if (currentState.equals(STATE.CONNECTED)) {
                        if (swaped) {  //zavijanje, dodamo 128
                            bluetoothSocket.getOutputStream().write((byte) 191);
                        } else {    //hitrost
                            bluetoothSocket.getOutputStream().write((byte) 63);
                        }

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        right = (Joystick) findViewById(R.id.joystick_right);
        right.setMotionConstraint(Joystick.MotionConstraint.HORIZONTAL);
        right.setJoystickListener(new JoystickListener() {
            @Override
            public void onDown() {
            }

            @Override
            public void onDrag(float degrees, float offset) {
                offsetR = (int) (63 *offset);
                angleR = (int) degrees;

                if (currentState.equals(STATE.CONNECTED)) {
                    updateJoystickData();
                }

                try {
                    if (currentState.equals(STATE.CONNECTED)) {
                        char posiljka;
                        if (!swaped) {  //zavijanje, dodamo 128
                            posiljka = (char) (offsetR * Math.cos(Math.toRadians(angleR))); //value from -63 to 63
                            posiljka += 128+63;
                            bluetoothSocket.getOutputStream().write(posiljka);
                        } else {    //hitrost
                            posiljka = (char) (offsetR * Math.sin(Math.toRadians(angleR)));
                            posiljka += 63;
                            bluetoothSocket.getOutputStream().write(posiljka);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onUp() {
                offsetR = 0;
                angleR = 0;

                if (currentState.equals(STATE.CONNECTED)) {
                    updateJoystickData();
                }

                try {
                    if (currentState.equals(STATE.CONNECTED)) {
                        if (!swaped) {  //zavijanje, dodamo 128
                            bluetoothSocket.getOutputStream().write((byte) 191);
                        } else {    //hitrost
                            bluetoothSocket.getOutputStream().write((byte) 63);
                        }

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        console = (ListView) findViewById(R.id.console);
        header = (TextView) findViewById(R.id.console_header);

        //bluetooth
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // Register for broadcasts on BluetoothAdapter state change
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        registerReceiver(mReceiver, filter);

        if(bluetoothAdapter == null){   //bluetooth adapter not available on this device
            msg("Bluetooth is not available on this device.");
            //close application
            finish();
        } else if (!bluetoothAdapter.isEnabled()) {
            header.setText("BLUETOOTH");
            currentState = STATE.BLUETOOTH_OFF;
            bluetoothOff();
        } else {
            header.setText("SELECT A DEVICE");
            currentState = STATE.BLUETOOTH_ON;
            selectDevice();
        }

    }

    @Override
    protected void onResume(){
        super.onResume();
    };

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // Unregister broadcast listeners
        unregisterReceiver(mReceiver);
    }

    public void bluetoothOff(){
        listItems = new ArrayList<String>(1);
        listItems.add("TURN ON");
        final ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1, listItems);
        console.setAdapter(adapter);
        console.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //send request to turn on bluetooth
                Intent turnOnBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(turnOnBluetooth, 1);
            }
        }); //Method called when the device from the list is clicked
    }

    public void selectDevice() {
        pairedDevices = bluetoothAdapter.getBondedDevices();

        listItems = new ArrayList<String>();
        if (pairedDevices.size()>0)
        {
            for(BluetoothDevice bt : pairedDevices)
            {
                listItems.add(bt.getName() + "\n" + bt.getAddress()); //Get the device's name and the address
            }

            final ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1, listItems);
            console.setAdapter(adapter);
            console.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    // Get the device MAC address, the last 17 chars in the View
                    String info = ((TextView) view).getText().toString();
                    deviceAddress = info.substring(info.length() - 17);

                    new ConnectBT().execute(); //Call the class to connect

                }
            }); //Method called when the device from the list is clicked

        }
        else
        {
            listItems.add("REFRESH");
            final ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1, listItems);
            console.setAdapter(adapter);
            console.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    selectDevice();
                }
            }); //Method called when the device from the list is clicked
            msg("No Paired Bluetooth Devices Found.");
        }


    }

    public void bluetoothConnected(){
        listItems = new ArrayList<String>(1);
        listItems.add("OFFSET - L: "+offsetL);
        listItems.add("ANGLE - L: "+angleL+"°");
        listItems.add("OFFSET - R: "+offsetR);
        listItems.add("ANGLE - R: "+angleR+"°");
        final ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1, listItems);
        console.setAdapter(adapter);
    }

    private void updateJoystickData() {
        if (angleL < 0) {
            angleL += 360;
        }
        if (angleR < 0) {
            angleR += 360;
        }
        listItems = new ArrayList<String>(1);
        listItems.add("OFFSET - L: "+offsetL);
        listItems.add("ANGLE - L: "+angleL+"°");
        listItems.add("OFFSET - R: "+offsetR);
        listItems.add("ANGLE - R: "+angleR+"°");
        final ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1, listItems);
        console.setAdapter(adapter);
    }

    private class ConnectBT extends AsyncTask<Void, Void, Void>  // UI thread
    {
        private boolean ConnectSuccess = true; //if it's here, it's almost connected

        @Override
        protected void onPreExecute() {
            progress = ProgressDialog.show(MainActivity.this, "Connecting...", "Please wait.");  //show a progress dialog
        }

        @Override
        protected Void doInBackground(Void... devices) { //while the progress dialog is shown, the connection is done in background
            try
            {
                if (bluetoothSocket == null || !isBtConnected)
                {
                    bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();//get the mobile bluetooth device
                    BluetoothDevice dispositivo = bluetoothAdapter.getRemoteDevice(deviceAddress);//connects to the device's address and checks if it's available
                    bluetoothSocket = dispositivo.createInsecureRfcommSocketToServiceRecord(myUUID);//create a RFCOMM (SPP) connection
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    bluetoothSocket.connect();//start connection
                }
            }
            catch (IOException e)
            {
                ConnectSuccess = false;//if the try failed, you can check the exception here
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) { //after the doInBackground, it checks if everything went fine
            super.onPostExecute(result);

            if (!ConnectSuccess) {
                msg("Connection Failed. Have you tried turning bluetooth on and off again?");
            } else {
                msg("Connected.");
                isBtConnected = true;
            }
            progress.dismiss();
        }
    }

    // fast way to call Toast
    private void msg(String s) {
        Toast.makeText(getApplicationContext(),s, Toast.LENGTH_LONG).show();
    }
}
